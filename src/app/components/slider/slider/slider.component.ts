import { Component, OnInit } from '@angular/core';
import { SliderService } from '@musBase/services/slider.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  NumberOfSlides = 3;
  NumberToScroll = 1;
  slides: any;

  slideConfig = {
    slidesToShow: this.NumberOfSlides,
    slidesToScroll: this.NumberToScroll
  };

  constructor( public sliderService: SliderService ) {}

  ngOnInit() {
    this.slides = this.sliderService.slides;
  }

  // addSlide() {
  //   // this.slides.push({img: 'assets/photo/studio/0.jpg', info: {price: 0}});
  // }
  //
  // removeSlide() {
  //   // this.slides.length = this.slides.length - 1;
  // }
  //
  // slickInit(e) {
  //   // console.log('slick initialized');
  // }
  //
  // breakpoint(e) {
  //   // console.log('breakpoint');
  // }
  //
  // afterChange(e) {
  //   // console.log('afterChange');
  // }
  //
  // beforeChange(e) {
  //   // console.log('beforeChange');
  // }
}
