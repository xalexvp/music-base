import { Component } from '@angular/core';
import { SelectedService } from '../../services/selected.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html'
})
export class NewsComponent {

  constructor( public selectedService: SelectedService ) { }

}
