import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PresentsComponent } from './presents.component';

describe('PresentsComponent', () => {
  let component: PresentsComponent;
  let fixture: ComponentFixture<PresentsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PresentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
