import { Component } from '@angular/core';
import { SelectedService } from '../../services/selected.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html'
})
export class PortfolioComponent {

  constructor( public selectedService: SelectedService ) { }
}
