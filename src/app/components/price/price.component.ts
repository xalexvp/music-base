import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { AllCommunityModules } from '@ag-grid-community/all-modules';
import { SelectedService } from '../../services/selected.service';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit {
  public modules = AllCommunityModules;
  private defaultColDef;
  private getRowHeight;
  private domLayout;
  columnDefs1: any;
  columnDefs2: any;
  rowData1: any;
  rowData2: any;
  tablesViewIndex = 3;

  constructor( private http: HttpClient,
               public selectedService: SelectedService ) {
    this.defaultColDef = {
      resizable: true
    };
    this.domLayout = 'autoHeight';
  }

  ngOnInit() {
    this.getData('services').subscribe(resp => {
      if (resp.titles) {
        this.columnDefs1 = [];
        resp.titles.map(title => {
          if (title.name && title.field) {
            this.columnDefs1.push(
              {
                headerName: title.name,
                width: title.width,
                autoHeight: true,
                field: title.field
              }
            );
          }
        });
      }
      if (resp.data) {
        this.rowData1 = resp.data;
      }
    });
    this.getData('lessons').subscribe(resp => {
      if (resp.titles) {
        this.columnDefs2 = [];
        resp.titles.map(title => {
          if (title.name && title.field) {
            this.columnDefs2.push(
              {
                headerName: title.name,
                autoHeight: true,
                width: title.width,
                field: title.field
              }
            );
          }
        });
      }
      if (resp.data) {
        this.rowData2 = resp.data;
      }
    });
  }

  getData(dataName) {
    return this.http.get( `assets/JSONs/${dataName}.json`)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          // this.errorsService.showError(err.error);
          return new Promise(() => err.error);
        }),
        map((result: any) => {

          if (!result) {
            // this.errorsService.showError({'error': 'Error', 'message': 'Немає відповіді від сервера', 'code': '01234'});
            return {};

          } else {
            return result;
          }
        })
      );
  }

  onGridReady(params) {
    const gridApi = params.api;
    gridApi.setDomLayout('autoHeight');
    window.addEventListener('resize', () => this.sizeToFit(gridApi));
    this.sizeToFit(gridApi);
    // setTimeout(() => this.sizeToFit(gridApi), 0);
  }
  sizeToFit(gridApi) {
    setTimeout(() => gridApi.sizeColumnsToFit(), 0);
    setTimeout(() => gridApi.resetRowHeights(), 0);
  }
}
