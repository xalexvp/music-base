import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ToolsConditionsComponent } from './tools-conditions.component';

describe('ToolsConditionsComponent', () => {
  let component: ToolsConditionsComponent;
  let fixture: ComponentFixture<ToolsConditionsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolsConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolsConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
