import { Component } from '@angular/core';
import { SelectedService } from '../../services/selected.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent {

  constructor( private selectedService: SelectedService ) { }

  goToStudio() {
    this.selectedService.setCurrent(1);
  }
}
