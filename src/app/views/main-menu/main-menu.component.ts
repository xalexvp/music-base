import { Component } from '@angular/core';
import { SelectedService } from '../../services/selected.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html'
})
export class MainMenuComponent {
  sliderViewIndex = 7;

  constructor( public selectedService: SelectedService ) { }
}
