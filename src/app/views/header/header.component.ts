import { Component } from '@angular/core';
import { SelectedService } from '../../services/selected.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  constructor( private selectedService: SelectedService ) { }

  goToMain() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
    this.selectedService.setCurrent(0);
  }
}
