import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule  } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { AgGridModule } from '@ag-grid-community/angular';
import { SliderModule } from './components/slider/slider.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './views/header/header.component';
import { MainMenuComponent } from './views/main-menu/main-menu.component';
import { AsideComponent } from './views/aside/aside.component';
import { FooterMenuComponent } from './views/footer-menu/footer-menu.component';
import { FooterComponent } from './views/footer/footer.component';
import { MainContentComponent } from './views/main-content/main-content.component';
import { PriceComponent } from './components/price/price.component';
import { ToolsConditionsComponent } from './components/tools-conditions/tools-conditions.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { VoicingsComponent } from './components/voicings/voicings.component';
import { NewsComponent } from './components/news/news.component';
import { PresentsComponent } from './components/presents/presents.component';
import { PromoComponent } from './components/promo/promo.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainMenuComponent,
    AsideComponent,
    FooterMenuComponent,
    FooterComponent,
    MainContentComponent,
    PriceComponent,
    ToolsConditionsComponent,
    PortfolioComponent,
    ClientsComponent,
    ContactsComponent,
    VoicingsComponent,
    NewsComponent,
    PresentsComponent,
    PromoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule,
    SliderModule,
    AgGridModule.withComponents([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
