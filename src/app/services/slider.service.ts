import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SliderService {
  slides = [
    {img: 'assets/photo/studio/0.jpg'},
    {img: 'assets/photo/studio/1.jpg'},
    {img: 'assets/photo/studio/2.jpg'},
    {img: 'assets/photo/studio/3.jpg'},
    {img: 'assets/photo/studio/4.jpg'},
    {img: 'assets/photo/studio/5.jpg'},
    {img: 'assets/photo/studio/6.jpg'},
    {img: 'assets/photo/studio/7.jpg'},
    {img: 'assets/photo/studio/8.jpg'},
    {img: 'assets/photo/studio/9.jpg'},
    {img: 'assets/photo/studio/10.jpg'},
    {img: 'assets/photo/studio/11.jpg'},
    {img: 'assets/photo/studio/12.jpg'}
  ];
}
