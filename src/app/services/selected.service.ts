import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SelectedService {
  selectedIndex = 0;
  loaded;

  setCurrent(index) {
    this.selectedIndex = index;
  }
  setNewIndex(event) {
    this.selectedIndex = event;
    if (this.selectedIndex > 1) {
      this.loaded = true;
    }
  }
}
